# easytrade



## Routes
<ul>
<li>First Page: http://easytrade.ge</a>
<br>
<h5>{{ need authentification }}</h5>
<li>Dashboard: http://easytrade.ge/dashboard </li>
<li>User Profile: http://easytrade.ge/user/userID</li>
<li>Company Profile: http://easytrade.ge/company/companyID</li>
<li>Show all tenders: http://easytrade.ge/tenders</li>
<li>Show specific tender: http://easytrade.ge/tender/tenderID</li>  
<li>Add new tender: http://easytrade.ge/addtender</li>
<li>Show all productions: http://easytrade.ge/orders</li>
<li>Add new pdocution: http://easytrade.ge/addproductions</li>
<li>Show all promotions: http://easytrade.ge/promotions</li>
<li>Show my promotions: http://easytrade.ge/mypromotions</li>
<li>Add new promotion: http://easytrade.ge/addpromotions</li>
<li>Log out: http://easytrade.ge/logout</li>
<br>
<h5>{{ Guest Mode (doesn't need authentification) }}</h5>
<li>Login: http://easytrade.ge/login</li>
<li>Registration: http://easytrade.ge/registration</li>

</ul>